//
//  FoodMenuViewController.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FoodMenuViewController.h"

@interface FoodMenuViewController ()

@end

@implementation FoodMenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initWithModeCustom:(bool)custom {
    customMode = custom;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(!customMode){
        self.title = @"Menu";
    } else {
        self.title = @"Select Item:";
    }
    listOfCategories = [[NSArray alloc] initWithObjects:@"Freshly Brewed Coffee",@"Espresso",@"Bagels & Donuts", @"Smoothies", nil];
    WholeMenu= [[NSMutableArray alloc] init];
    
    
    
    menuItem *menuitemOne = [[menuItem alloc] init];
    menuitemOne.itemName = @"A Coffee";
    menuitemOne.priceList = [NSArray arrayWithObjects:@"$4.50", nil];
    menuitemOne.numOfSizes = 1;
    
    menuItem *menuItemTwo = [[menuItem alloc] init];
    menuItemTwo.itemName = @"Decaf Coffee";
    menuItemTwo.priceList = [NSArray arrayWithObjects:@"$1.50", @"$1.75", @"$2.00", nil];
    menuItemTwo.numOfSizes = 3;
    
    menuItem *menuItemThree = [[menuItem alloc] init];
    menuItemThree.itemName = @"Espresso";
    menuItemThree.priceList = [NSArray arrayWithObjects:@"$4.50", nil];
    menuItemThree.numOfSizes = 1;
    
    menuItem *menuItemFour = [[menuItem alloc] init];
    menuItemFour.itemName = @"Poppyseed Bagel";
    menuItemFour.priceList = [NSArray arrayWithObjects:@"$1.75", nil];
    menuItemFour.numOfSizes = 1;
    menuItemFour.ingredientList = [[NSArray alloc]initWithObjects:@"Schmear", @"Toasted?", nil];
    NSArray *dorp = [NSArray arrayWithObjects:@"C.Cheese",@"Butter", @"None", nil];
    NSArray *darp = [NSArray arrayWithObjects:@"No", @"Yes", nil];
    menuItemFour.ingredientQuants = [NSArray arrayWithObjects:dorp, darp, nil];
    
    menuItem *menuItemSeven = [[menuItem alloc] init];
    menuItemSeven.itemName = @"Pesto Bagel";
    menuItemSeven.priceList = [NSArray arrayWithObjects:@"$2.50", nil];
    menuItemSeven.numOfSizes = 1;
    menuItemSeven.ingredientList = [[NSArray alloc]initWithObjects:@"Schmear", @"Toasted?", nil];
    //NSArray *dorp = [NSArray arrayWithObjects:@"C.Cheese",@"Butter", @"None", nil];
    //NSArray *darp = [NSArray arrayWithObjects:@"No", @"Yes", nil];
    menuItemSeven.ingredientQuants = [NSArray arrayWithObjects:dorp, darp, nil];
    
    menuItem *menuItemFive = [[menuItem alloc] init];
    menuItemFive.itemName = @"Maple Bar";
    menuItemFive.priceList = [NSArray arrayWithObjects:@"$1.50", nil];
    menuItemFive.numOfSizes = 1;
    
    menuItem *menuItemSix = [[menuItem alloc] init];
    menuItemSix.itemName = @"Strawberry Banana";
    menuItemSix.priceList = [NSArray arrayWithObjects:@"$1.75", @"$2.25", @"$2.75", nil];
    menuItemSix.numOfSizes = 3;
    menuItemSix.ingredientList = [NSArray arrayWithObjects:@"Boost", nil];
    NSArray *sixOne = [NSArray arrayWithObjects:@"None",@"Weight Loss", @"Energy", @"Immune", nil];
    menuItemSix.ingredientQuants = [NSArray arrayWithObjects:sixOne, nil];
    
    NSArray *countriesToLiveInArray = [NSArray arrayWithObjects:menuitemOne, menuItemTwo, nil];
    NSDictionary *countriesToLiveInDict = [NSDictionary dictionaryWithObject:countriesToLiveInArray forKey:@"Countries"];
    
    NSArray *countriesLivedInArray = [NSArray arrayWithObjects:menuItemThree, nil];
    NSDictionary *countriesLivedInDict = [NSDictionary dictionaryWithObject:countriesLivedInArray forKey:@"Countries"];
    
    NSArray *blah = [NSArray arrayWithObjects:menuItemFour, menuItemFive, menuItemSeven, nil];
    NSDictionary *stuff = [NSDictionary dictionaryWithObject:blah forKey:@"Countries"];
    
    NSArray *smooth = [NSArray arrayWithObjects:menuItemSix, nil];
    NSDictionary *smoothy = [NSDictionary dictionaryWithObject:smooth forKey:@"Countries"];
    
    [WholeMenu addObject:countriesToLiveInDict];
    [WholeMenu addObject:countriesLivedInDict];
    [WholeMenu addObject:stuff];
    [WholeMenu addObject:smoothy];
    
    
    [menuitemOne release];
    [menuItemTwo release];
    [menuItemThree release];
    [menuItemFour release];
    [menuItemFive release];
    [menuItemSix release];
    [menuItemSeven release];
    /*
    [countriesToLiveInArray release];
    [countriesToLiveInDict release];
    [countriesLivedInArray release];
    [countriesLivedInDict release];
    [blah release];
    [stuff release];
    [smooth release];
    [smoothy release];
    */
    
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) setModeCustom {
    NSLog(@"setModeCustom");
    customMode = YES;
    if(customMode == YES){
        NSLog(@"YES");
    } else {
        NSLog(@"NO");
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [WholeMenu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSMutableDictionary *dictionary = [WholeMenu objectAtIndex:section];
    NSArray *array = [dictionary objectForKey:@"Countries"];
    return [array count];
    
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *string = [listOfCategories objectAtIndex:section];
    return string;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Set up the cell...
    
    //First get the dictionary object
    NSMutableDictionary *dictionary = [WholeMenu objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"Countries"];
    menuItem *cellValue = [array objectAtIndex:indexPath.row];
    cell.text = cellValue.itemName;
    return cell;

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSMutableDictionary *dictionary = [WholeMenu objectAtIndex:indexPath.section];
    NSArray *array = [dictionary objectForKey:@"Countries"];
    menuItem *cellValue = [array objectAtIndex:indexPath.row];

    if (!customMode){
        NSLog(@"Mode is Standard");
        MenuItemViewController *mvc = [[MenuItemViewController alloc] initWithFoodItem:cellValue];
        [self.navigationController pushViewController:mvc animated:YES];
        [mvc release];
    } else {
        //[cellValue retain];
        NSLog(@"Mode is Custom");
        CustomizerViewController *cvc = [[CustomizerViewController alloc] initWithFoodItem:cellValue :customMode];
        NSLog(@"cvc declared");
       // [cvc setShownItem:cellValue];
       // NSLog(@"comedy setShownItem");
        [self.navigationController pushViewController:cvc animated:YES];
        NSLog(@"Pooshed");
        
        [cvc release];
    }
}
-(void) dealloc{
    [WholeMenu release];
    [listOfCategories release];

}

@synthesize customMode; 

@end
