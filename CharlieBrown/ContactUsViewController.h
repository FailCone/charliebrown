//
//  ContactUsViewController.h
//  CharlieBrown
//
//  Created by Charles Semple on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsViewController : UIViewController {
    
    IBOutlet UIButton *btnCallUs;
    
    IBOutlet UIButton *btnNavigate;
}
- (IBAction)didTapNavigate:(id)sender;
- (IBAction)didTapCall:(id)sender;

@end
