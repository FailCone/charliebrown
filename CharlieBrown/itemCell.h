//
//  itemCell.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface itemCell : UITableViewCell {
    
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *priceLabel;
     
    
}

@property(nonatomic, retain) IBOutlet UILabel *nameLabel;
@property(nonatomic, retain) IBOutlet UILabel *priceLabel;

@end
