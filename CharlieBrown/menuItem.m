//
//  menuItem.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "menuItem.h"

@implementation menuItem

- (id) init {
    
    itemName = @"";
    priceList = [[NSArray alloc] init];
    ingredientList = [NSArray arrayWithObjects: nil];
    ingredientQuants = [NSArray arrayWithObjects: nil];
    numOfSizes = 0;
    
    return self;
}

-(NSString *)priceText {
    NSString *displayString;
    
    if(self.numOfSizes < 2){
        displayString = [self.priceList objectAtIndex:0];
    } else {
        if(self.numOfSizes == 2){
            displayString = [NSString stringWithFormat:@"Sngl: %@ | Dbl: %@", [self.priceList objectAtIndex:0], [self.priceList objectAtIndex:1]];
        } else {
            displayString = [NSString stringWithFormat:@"Sm: %@ | Md: %@ | Lg: %@", [self.priceList objectAtIndex:0], [self.priceList objectAtIndex:1], [self.priceList objectAtIndex:2]];
        }
    }
    return displayString;
}
    

@synthesize itemName;
@synthesize priceList;
@synthesize ingredientList;
@synthesize ingredientQuants;
@synthesize numOfSizes;

@end
