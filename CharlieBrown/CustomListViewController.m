//
//  CustomListViewController.m
//  CharlieBrown
//
//  Created by Charles Semple on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomListViewController.h"

@interface CustomListViewController ()

@end

@implementation CustomListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Custom Items";
    custTable.delegate = self;
    custTable.dataSource = self;
    CartParser *parser = [[CartParser alloc]init];
    itemsToShow = [parser loadCustomItems];
    if(itemsToShow == nil){
        itemsToShow = [[NSMutableArray alloc]init];
    }
    [parser release];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [self reloadItems];
}

- (void) reloadItems {
    CartParser *parser = [[CartParser alloc]init];
    itemsToShow = [parser loadCustomItems];
    if(itemsToShow == nil){
        itemsToShow = [[NSMutableArray alloc]init];
    }
    [parser release];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [itemsToShow count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cartItem *tempItem = [itemsToShow objectAtIndex:indexPath.row];
    CartViewController *cvc = [[CartViewController alloc]initWithCartItem:tempItem];
    [self.navigationController pushViewController:cvc animated:YES];
    [cvc release];
    return;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CartCell";
    CartCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //assert([item.ingredientList objectAtIndex:indexPath.row]);
    // Configure the cell...
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CartCell" owner:nil options:nil];
        
        for (id currentObject in topLevelObjects){
            if([currentObject isKindOfClass:[UITableViewCell class]]){
                cell = (CartCell *) currentObject;
                break;
            }
        }
        
    }
    cartItem *tempItem = [itemsToShow objectAtIndex:indexPath.row];
    
    cell.lblName.text = tempItem.itemName;
    cell.lblPrice.text = tempItem.itemPrice;
    cell.lblChanges.text = [tempItem customString];
    
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [itemsToShow removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    
}


-(void)viewDidDisappear:(BOOL)animated {
    //SAVE
    CartParser *parser = [[CartParser alloc]init];
    [parser saveCustomItemsArray:itemsToShow appending:NO];
    [parser release];
}

- (void)viewDidUnload
{
    [btnAddBtn release];
    btnAddBtn = nil;
    [custTable release];
    custTable = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)dealloc {
    [btnAddBtn release];
    [custTable release];
    [super dealloc];
}
//Make a new menu interface tree to add a thing to the list.
- (IBAction)didTapAddBtn:(id)sender {
    CBMenu *WholeMenu = [CBMenu sharedInstance];
    NSArray *cats = [WholeMenu returnCategories];
    NSArray *menu = [WholeMenu returnAllItems];
    [cats retain];
    [menu retain];
    FoodMenuTwoViewController *mmvc = [[FoodMenuTwoViewController alloc]initWithData:cats :YES :YES];
    [mmvc setDataSource:cats];  
    [mmvc setMenu:menu];
    [mmvc setTitle:@"Menu"];
    [self.navigationController pushViewController:mmvc animated:YES];
    [mmvc release];
    [cats release];
    [menu release];
    //FOR SOME REASON IT DOESN'T LIKE ME RELEASING CATS AND MENU OR REMOVING THEIR
    //RETAIN STATEMENTS ABOVE PLEASE DON'T HURT ME.
    
    return;    
}
@end
