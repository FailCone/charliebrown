//
//  CategoryCell.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UITableViewCell {
    
    IBOutlet UILabel *displayedCategory;
    
}

@property(nonatomic, retain) IBOutlet UILabel *displayedCategory;
@end
