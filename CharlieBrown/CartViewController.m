//
//  CartViewController.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CartViewController.h"

@interface CartViewController ()

@end

@implementation CartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initWithCartItem:(cartItem *)inItem {
    CartParser *parser = [[CartParser alloc]init];
    itemsInCart = [parser loadCart];
    if(itemsInCart == nil){
        itemsInCart = [[NSMutableArray alloc]init];
    }
    if(inItem != nil){
        newItemToAdd = inItem;
        [itemsInCart addObject:inItem];
    }
    return self;
}

- (void)updateCart {
    [itemsInCart addObject:newItemToAdd];

    return;
}

- (IBAction)didTapOrder:(id)sender {
    if([itemsInCart count] == 0){
        UIAlertView *NoStuff =[[UIAlertView alloc] initWithTitle:@"ERROR:" message:@"Can't place an empty order" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [NoStuff show];
        [NoStuff release];
        return;
    }
    if([txtNameField.text length] != 0){
        CartParser *parser = [[CartParser alloc]init];
        [parser saveCartWithArray:itemsInCart withName:txtNameField.text];
        
        //SENDING STUFF GOES HERE!!!!!!!
        
        
        UIAlertView *OrderSubmitted =[[UIAlertView alloc] initWithTitle:@"Order Submitted" message:@"Your order has been placed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [OrderSubmitted show];
        [OrderSubmitted release];
        [itemsInCart removeAllObjects];
        [self goHome];
        return;
    } 
    UIAlertView *NoName =[[UIAlertView alloc] initWithTitle:@"ERROR:" message:@"Please input a name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [NoName show];
    [NoName release];
    return;

}

- (IBAction)didTapHome:(id)sender {
    [self goHome];
}

- (IBAction)didTapAdd:(id)sender {
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    assert(txtNameField);
    txtNameField.delegate = self;
    tblCartTable.delegate = self;
    tblCartTable.dataSource = self;
    self.title = @"Your Cart";
    // Do any additional setup after loading the view from its nib.
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [itemsInCart count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //I don't think I want anything here
    return;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CartCell";
    CartCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //assert([item.ingredientList objectAtIndex:indexPath.row]);
    // Configure the cell...
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CartCell" owner:nil options:nil];
        
        for (id currentObject in topLevelObjects){
            if([currentObject isKindOfClass:[UITableViewCell class]]){
                cell = (CartCell *) currentObject;
                break;
            }
        }
        
    }
    cartItem *tempItem = [itemsInCart objectAtIndex:indexPath.row];
    
    cell.lblName.text = tempItem.itemName;
    cell.lblPrice.text = tempItem.itemPrice;
    cell.lblChanges.text = [tempItem customString];
    
    return cell;
}

-(void) goHome {
    NSMutableArray *vcs = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for(int i = [vcs count]-1; i > 0; i--){
        [vcs removeObjectAtIndex:i];
    }
    self.navigationController.viewControllers = vcs;
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


-(void)viewDidDisappear:(BOOL)animated {
    //SAVE
    CartParser *parser = [[CartParser alloc]init];
    [parser saveCartWithArray:itemsInCart withName:nil];
}


- (void)viewDidUnload
{
    [tblCartTable release];
    tblCartTable = nil;
    [txtNameField release];
    txtNameField = nil;
    [btnOrderbtn release];
    btnOrderbtn = nil;
    [btnHomeBtn release];
    btnHomeBtn = nil;
    [btnAddBtn release];
    btnAddBtn = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
     if (editingStyle == UITableViewCellEditingStyleDelete) {
         // Delete the row from the data source
         [itemsInCart removeObjectAtIndex:indexPath.row];
         [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   

 }
 


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [tblCartTable release];
    [txtNameField release];
    [btnOrderbtn release];
    [btnHomeBtn release];
    [btnAddBtn release];
    [super dealloc];
}
@end
