//
//  MenuItemViewController.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "menuItem.h"
#import "CustomizerViewController.h"

@interface MenuItemViewController : UIViewController {
    
    menuItem *food;
    IBOutlet UILabel *lblFoodTitle;
    IBOutlet UIImageView *imgFoodPicture;
    IBOutlet UILabel *lblPrice;
    IBOutlet UIButton *btnOrderBtn;
}

- (id) initWithFoodItem:(menuItem *) item;
- (void) setPriceText;
- (IBAction)didTapCartBtn:(id)sender;



@end
