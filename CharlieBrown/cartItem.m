//
//  cartItem.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cartItem.h"

@implementation cartItem

- (id) init {
    
    changeList = [[NSMutableArray alloc]init];
    
    
    return self;
}

-(NSString *)customString {
    if([changeList count] < 1) {
        return nil;
    } else {
        NSString *customString = [changeList objectAtIndex:0];
        for(int i = 1; i < [changeList count];i++){
           customString = [customString stringByAppendingString:[NSString stringWithFormat:@"\n%@", [changeList objectAtIndex:i]]];

        }
        return customString;
    }
    
}

@synthesize itemName;
@synthesize itemPrice;
@synthesize custName;
@synthesize changeList;
@end
