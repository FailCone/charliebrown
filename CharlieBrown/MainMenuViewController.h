//
//  MainMenuViewController.h
//  CharlieBrown
//
//  Created by Charles Semple on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBMenu.h"
#import "FoodMenuTwoViewController.h"
#import "CustomListViewController.h"
#import "ContactUsViewController.h"
#import "CartViewController.h"

@interface MainMenuViewController : UIViewController {
    
    //Button/Icon to open the food menuu
    IBOutlet UIImageView *imvFoodMenuIcon;
    //Button/Icon to open the Custom Item list
    IBOutlet UIImageView *imvCustomMenuIcon;
    //Button/Icon to open the shopping cart
    IBOutlet UIImageView *imvOrderMenuIcon;
    //Button/Icon to open the Contact Us screen.
    IBOutlet UIImageView *imvContactMenuIcon;

}

@end
