//
//  CartViewController.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cartItem.h"
#import "CartCell.h"
#import "CartParser.h"

@interface CartViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
    
    IBOutlet UITableView *tblCartTable;
    
    NSMutableArray *itemsInCart;
    cartItem *newItemToAdd;
    IBOutlet UITextField *txtNameField;
    
    IBOutlet UIButton *btnOrderbtn;
    
    IBOutlet UIButton *btnAddBtn;
    IBOutlet UIButton *btnHomeBtn;
}

- (id) initWithCartItem:(cartItem *)inItem;
- (void) updateCart;
- (IBAction)didTapOrder:(id)sender;
- (IBAction)didTapHome:(id)sender;
- (IBAction)didTapAdd:(id)sender;
- (void)goHome;


@end
