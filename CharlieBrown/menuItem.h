//
//  menuItem.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface menuItem : NSObject {
    //Item's Name
    NSString *itemName;
    //String of Prices - "$4.50" or "$1.50 / $2.50 / $3.50"
    NSArray *priceList;
    //List of Ingredients 
    NSArray *ingredientList;
    //List of adjusted Quantities
    NSArray *ingredientQuants; 
    //Number of Sizes for corresponding prices
    int numOfSizes;
    
    
}

- (NSString *)priceText;
@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSArray *priceList;
@property (nonatomic, copy) NSArray *ingredientList;
@property (nonatomic, copy) NSArray *ingredientQuants;
@property (nonatomic, assign) int numOfSizes;

@end
