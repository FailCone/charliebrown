//
//  FoodMenuTwoViewController.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "menuItem.h"
#import "CustomizerViewController.h"
#import "MenuItemViewController.h"
#import "CategoryCell.h"
#import "itemCell.h"

@interface FoodMenuTwoViewController : UITableViewController {
    
NSArray *dataSource;
NSArray *StoredMenu;

enum menuMode *mode;
bool isCustom;
bool showsCategories;


}

//Set datasource and if, later down the line, we're adding to the cart or adding to the 
//list of custom items
- (id) initWithData:(NSArray*) inArray:(bool) asCustom:(bool)ShowsCats;
- (void) setDataSource:(NSArray *) inArray; //REDUNDANCE
- (void) setMenu:(NSArray *) inArray; //Stash the whole menu so that we can pull it up.



@end
