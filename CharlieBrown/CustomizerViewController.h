//
//  CustomizerViewController.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomizerCell.h"
#import "CartViewController.h"
#import "cartItem.h"
#import "CartParser.h"
#import "CustomListViewController.h"

@interface CustomizerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UITableView *custTable;
    
    bool customizerMode;
    IBOutlet UIButton *btnCancelBtn;
    IBOutlet UISegmentedControl *segSizeSelect;
    IBOutlet UITextField *txtCustomName;
    IBOutlet UILabel *lblNameLabel;
    IBOutlet UIButton *btnConfirmBtn;
    

    int sizeSelected;
    menuItem *item;
    NSMutableArray *cellStorage;
    
}
//Init with a food item and if we're storing it in the list of custom items or the cart
- (id) initWithFoodItem:(menuItem *)inputItem:(bool)inCustomMode;
- (IBAction)didTapCancelBtn:(id)sender;
- (IBAction)didTapAddBtn:(id)sender;

-(cartItem *)createCartItem;

@property(nonatomic,retain) IBOutlet UITableView *custTable;
@end
