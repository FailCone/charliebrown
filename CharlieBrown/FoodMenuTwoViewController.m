//
//  FoodMenuTwoViewController.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FoodMenuTwoViewController.h"

@interface FoodMenuTwoViewController ()

@end

@implementation FoodMenuTwoViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    return;
}

-(id)initWithData:(NSArray *)inArray :(bool)asCustom:(bool)ShowsCats{ 
    dataSource = inArray;
    isCustom = asCustom;
    showsCategories = ShowsCats;
    return self;
}

- (void)setDataSource:(NSArray *)inArray{
    dataSource = inArray;
}
- (void)setMenu:(NSArray *)inArray {
    StoredMenu = inArray;
    [StoredMenu retain];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Configure the cell...
    if(showsCategories){
        static NSString *CellIdentifier = @"CategoryCell";
        CategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(cell == nil){
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CategoryCell" owner:nil options:nil];
            
            for (id currentObject in topLevelObjects){
                if([currentObject isKindOfClass:[UITableViewCell class]]){
                    cell = (CategoryCell *) currentObject;
                    break;
                }
            }
            
        }
        NSString *cellValue = [dataSource objectAtIndex:indexPath.row];
        cell.displayedCategory.text = cellValue;
        return cell;
    } else {
        static NSString *CellIdentifier = @"itemCell";
        itemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil){
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"itemCell" owner:nil options:nil];
            
            for (id currentObject in topLevelObjects){
                if([currentObject isKindOfClass:[UITableViewCell class]]){
                    cell = (itemCell *) currentObject;
                    break;
                }
            }
        }
        
        menuItem *cellValue = [dataSource objectAtIndex:indexPath.row];
        cell.nameLabel.text = cellValue.itemName;
        cell.priceLabel.text = [cellValue priceText];
        return cell;
    }

    
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(showsCategories){ 
        NSArray *tempMenu =  [StoredMenu objectAtIndex:indexPath.row];
        FoodMenuTwoViewController *mmvc = [[FoodMenuTwoViewController alloc]initWithData:tempMenu :isCustom :NO];
        [mmvc setTitle:[dataSource objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:mmvc animated:YES];
        [mmvc release];
        return;
    } else { 
        
        menuItem *tempItem = [dataSource objectAtIndex:indexPath.row];
        if(!isCustom){
            MenuItemViewController *dvc = [[MenuItemViewController alloc]initWithFoodItem:tempItem];
            [dvc setTitle:tempItem.itemName];
            [self.navigationController pushViewController:dvc animated:YES];
            [dvc release];
            return;
        } else { 
            CustomizerViewController *cvc = [[CustomizerViewController alloc]initWithFoodItem:tempItem:YES];
            [cvc setTitle:@"Customize"];
            [self.navigationController pushViewController:cvc animated:YES];
            [cvc release];
            return;
        }
        
    }
        
}

@end
