//
//  MenuItemViewController.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuItemViewController.h"

@interface MenuItemViewController ()

@end

@implementation MenuItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    lblFoodTitle.text = food.itemName;
    [self setPriceText];
    
}


- (id) initWithFoodItem:(menuItem *)item{
    food = item;
    

    return self;
}

-(void) setPriceText {
    NSString *displayString = [[NSString alloc] init];
    
    if(food.numOfSizes < 2){
        displayString = [[NSString alloc] initWithString:[food.priceList objectAtIndex:0]];
    } else {
        if(food.numOfSizes == 2){
            displayString = [NSString stringWithFormat:@"Sngl: %@ - Dbl: %@", [food.priceList objectAtIndex:0], [food.priceList objectAtIndex:1]];
        } else {
            displayString = [NSString stringWithFormat:@"Sm: %@ - Md: %@ - Lg: %@", [food.priceList objectAtIndex:0], [food.priceList objectAtIndex:1], [food.priceList objectAtIndex:2]];
        }
    }
    lblPrice.text = displayString;
    
}

- (IBAction)didTapCartBtn:(id)sender {
    CustomizerViewController *cvc = [[CustomizerViewController alloc] initWithFoodItem:food :NO];
    [self.navigationController pushViewController:cvc animated:YES];
    
    [cvc release];
}

- (void)viewDidUnload
{
    [lblFoodTitle release];
    lblFoodTitle = nil;
    [imgFoodPicture release];
    imgFoodPicture = nil;
    [lblPrice release];
    lblPrice = nil;
    [btnOrderBtn release];
    btnOrderBtn = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [lblFoodTitle release];
    [imgFoodPicture release];
    [lblPrice release];
    [btnOrderBtn release];
    [super dealloc];
}
@end
