//
//  CartCell.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cartItem.h"

@interface CartCell : UITableViewCell { 
 
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblPrice;

    IBOutlet UILabel *lblChanges;
}

//Initing cartCell with a cartItem.
-(id) initWithCartItem:(cartItem *) item;

@property(nonatomic, retain) IBOutlet UILabel *lblName;
@property(nonatomic, retain) IBOutlet UILabel *lblPrice;
@property(nonatomic, retain) IBOutlet UILabel *lblChanges;
@end
