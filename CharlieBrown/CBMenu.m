//
//  CBMenu.m
//  MenuViewTwoTest
//
//  Created by Charles Semple on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CBMenu.h"

static CBMenu *sharedInstance = nil;

@implementation CBMenu


- (id) init {
    
    if ((self = [super init]) == nil) return nil;
    
    if(categories == nil || items == nil){
        [self loadMenu];
    }
    
    return self;
}

-(NSArray *)returnCategories{
    if(categories == nil){
        [self loadMenu];
    }
    return categories;
}

-(NSArray *)returnItemsWithCategory:(int)idx{
    if(items == nil){
        [self loadMenu];
    }
    NSArray *tempArr = [items objectAtIndex:idx];
    return tempArr;
}

-(NSArray *)returnAllItems {
    return items;
}

- (void) loadMenu { 
    if(categories && items){ 
        return;
    }
    categories = [[NSArray alloc] initWithObjects:@"Coffee", @"Coffee Alternatives", @"Smoothies", @"Baked Goods", @"Sandwiches",@"Salads", nil];
    
    NSArray *nyQuants = [[NSArray alloc] initWithObjects:@"No", @"Yes", nil];
    //NSArray *ynQuants = [NSArray arrayWithObjects:@"Yes", @"No", nil];
    NSArray *numQuants = [[NSArray alloc] initWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"0", nil];
    NSArray *espressoIList = [[NSArray alloc] initWithObjects:@"Decaf", @"Iced", @"Soymilk", @"Shots",nil];
    NSArray *sixOne = [[NSArray alloc] initWithObjects:@"None",@"Weight Loss", @"Energy", @"Immune", nil];

    
    menuItem *menuitemOne = [[menuItem alloc] init];
    menuitemOne.itemName = @"Regular Coffee";
    menuitemOne.priceList = [NSArray arrayWithObjects:@"$1.50",@"$1.95",@"$2.15", nil];
    menuitemOne.numOfSizes = 3;
    // menuitemOne.ingredientList = [NSArray arrayWithObjects:@"Decaf", @"Iced", nil];
    //menuitemOne.ingredientQuants = [NSArray arrayWithObjects:nyQuants, nyQuants, nil];
    
    menuItem *menuItemTwo = [[menuItem alloc] init];
    menuItemTwo.itemName = @"Latte";
    menuItemTwo.priceList = [NSArray arrayWithObjects:@"$2.55", @"$3.45", @"$3.75", nil];
    menuItemTwo.numOfSizes = 3;
    menuItemTwo.ingredientList = espressoIList;
    menuItemTwo.ingredientQuants = [NSArray arrayWithObjects:nyQuants, nyQuants, nyQuants, numQuants, nil];
    
    menuItem *mocha = [[menuItem alloc]init];
    mocha.itemName = @"Mocha";
    mocha.priceList = [NSArray arrayWithObjects:@"$3.15",@"$3.95",@"$4.75", nil];
    mocha.numOfSizes = 3;
    //mocha.ingredientList = espressoIList;    
    //THIS LINE BREAKS THE ENTIRE PROGRAM
    //mocha.ingredientQuants = [NSArray arrayWithObjects:nyQuants, nyQuants, nyQuants, numQuants, nil];

    
    menuItem *cappuccino = [[menuItem alloc]init];
    cappuccino.itemName = @"Cappuccino";
    cappuccino.priceList = [NSArray arrayWithObjects:@"$2.55",@"$3.45",@"$3.75", nil];
    cappuccino.numOfSizes = 3;
    
    menuItem *americano = [[menuItem alloc]init];
    americano.itemName = @"Americano";
    americano.priceList = [NSArray arrayWithObjects:@"$1.95",@"$2.45",@"$2.95", nil];
    americano.numOfSizes = 3;
    
    menuItem *caramelMocha = [[menuItem alloc]init];
    caramelMocha.itemName = @"Caramel Mocha";
    caramelMocha.priceList = [NSArray arrayWithObjects:@"$3.45",@"$4.15",@"$4.55", nil];
    caramelMocha.numOfSizes = 3;
    
    menuItem *wCaramelMocha = [[menuItem alloc]init];
    wCaramelMocha.itemName = @"White Chocolate Mocha";
    wCaramelMocha.priceList = [NSArray arrayWithObjects:@"$3.15",@"$3.95",@"$4.75", nil];
    wCaramelMocha.numOfSizes = 3;
    
    menuItem *fraps = [[menuItem alloc]init];
    fraps.itemName = @"Fraps";
    fraps.priceList = [NSArray arrayWithObjects:@"$4.15", nil];
    fraps.numOfSizes = 1;
    //fraps.ingredientList = [NSArray arrayWithObjects:@"Coffee", nil];
     //NSArray *frapList = [[NSArray alloc] initWithObjects:@"Mocha", @"Caramel", @"W.Choc.", nil];
     //fraps.ingredientQuants = [NSArray arrayWithObjects:frapList, nil];
    
    
    NSArray *coffeeArray = [NSArray arrayWithObjects:menuitemOne, menuItemTwo, mocha, cappuccino, americano, caramelMocha, wCaramelMocha, fraps,  nil];
    
    [fraps release];
    [wCaramelMocha release];
    [caramelMocha release];
    [americano release];
    [cappuccino release];
    [mocha release];
    [menuItemTwo release];
    [menuitemOne release];
    
    
    menuItem *menuItemThree = [[menuItem alloc] init];
    menuItemThree.itemName = @"Chai Latte";
    menuItemThree.priceList = [NSArray arrayWithObjects:@"$2.90", @"$3.55", @"3.95", nil];
    menuItemThree.numOfSizes = 3;
    
    menuItem *hotStashTea = [[menuItem alloc] init];
    hotStashTea.itemName = @"Hot Stash Tea";
    hotStashTea.priceList = [NSArray arrayWithObjects:@"$1.50", @"$2.15", @"2.45", nil];
    hotStashTea.numOfSizes = 3;
    
    menuItem *specialTea = [[menuItem alloc] init];
    specialTea.itemName = @"Specialty Tea";
    specialTea.priceList = [NSArray arrayWithObjects:@"$1.50", @"$2.15", @"2.45", nil];
    specialTea.numOfSizes = 3;
    
    menuItem *brewedTea = [[menuItem alloc] init];
    brewedTea.itemName = @"Specialty Tea";
    brewedTea.priceList = [NSArray arrayWithObjects:@"$1.50", @"$2.15", @"2.45", nil];
    brewedTea.numOfSizes = 3;
    
    menuItem *steamingCider = [[menuItem alloc] init];
    steamingCider.itemName = @"Steamed Hot Cider";
    steamingCider.priceList = [NSArray arrayWithObjects:@"$2.55", @"$2.80", @"3.10", nil];
    steamingCider.numOfSizes = 3;
    
    menuItem *caramelCider = [[menuItem alloc] init];
    caramelCider.itemName = @"Hot Caramel Cider";
    caramelCider.priceList = [NSArray arrayWithObjects:@"$3.05", @"$3.30", @"$3.60", nil];
    caramelCider.numOfSizes = 3;
    
    menuItem *hotChocolate = [[menuItem alloc] init];
    hotChocolate.itemName = @"Hot Chocolate";
    hotChocolate.priceList = [NSArray arrayWithObjects:@"$2.45", @"$2.65", @"$3.15", nil];
    hotChocolate.numOfSizes = 3;
    
    menuItem *steamedMilk = [[menuItem alloc] init];
    steamedMilk.itemName = @"Steamed Milk";
    steamedMilk.priceList = [NSArray arrayWithObjects:@"$1.50", @"$2.15", @"$2.45", nil];
    steamedMilk.numOfSizes = 3;
    
    NSArray *espressoArray = [NSArray arrayWithObjects:menuItemThree, hotStashTea, specialTea, brewedTea, steamingCider,caramelCider, hotChocolate,steamedMilk, nil];
    
    [steamedMilk release];
    [hotChocolate release];
    [caramelCider release];
    [steamingCider release];
    [brewedTea release];
    [specialTea release];
    [hotStashTea release];
    [menuItemThree release];
    
    menuItem *menuItemFour = [[menuItem alloc] init];
    menuItemFour.itemName = @"Regular Bagel";
    menuItemFour.priceList = [NSArray arrayWithObjects:@"$1.25", nil];
    menuItemFour.numOfSizes = 1;
    
    menuItem *menuItemSeven = [[menuItem alloc] init];
    menuItemSeven.itemName = @"Pizza Bagel";
    menuItemSeven.priceList = [NSArray arrayWithObjects:@"$2.00", nil];
    menuItemSeven.numOfSizes = 1;
    menuItemSeven.ingredientList = [[NSArray alloc]initWithObjects:@"Schmear", @"Toasted?", nil];
    NSArray *dorp = [[NSArray alloc]initWithObjects:@"C.Cheese",@"Butter", @"None", nil];
    NSArray *darp = [[NSArray alloc]initWithObjects:@"No", @"Yes", nil];
    menuItemSeven.ingredientQuants = [NSArray arrayWithObjects:dorp, darp, nil];
    
    menuItem *menuItemFive = [[menuItem alloc] init];
    menuItemFive.itemName = @"Pesto Bagel";
    menuItemFive.priceList = [NSArray arrayWithObjects:@"$2.00", nil];
    menuItemFive.numOfSizes = 1;
    menuItemFive.ingredientList = [[NSArray alloc]initWithObjects:@"Schmear", @"Toasted?", nil];
    menuItemFive.ingredientQuants = [NSArray arrayWithObjects:dorp, darp, nil];
    
    [dorp release];
    [darp release];
    
    menuItem *sunCake = [[menuItem alloc] init];
    sunCake.itemName = @"Suncake";
    sunCake.priceList = [NSArray arrayWithObjects:@"$2.25", nil];
    sunCake.numOfSizes = 1;
    
    menuItem *croissant = [[menuItem alloc] init];
    croissant.itemName = @"Croissant";
    croissant.priceList = [NSArray arrayWithObjects:@"$2.25", nil];
    croissant.numOfSizes = 1;
    
    menuItem *muffin = [[menuItem alloc] init];
    muffin.itemName = @"Muffin";
    muffin.priceList = [NSArray arrayWithObjects:@"$2.25", nil];
    muffin.numOfSizes = 1;
    
    menuItem *scone = [[menuItem alloc] init];
    scone.itemName = @"Scone";
    scone.priceList = [NSArray arrayWithObjects:@"$2.25", nil];
    scone.numOfSizes = 1;
    
    menuItem *fritter = [[menuItem alloc] init];
    fritter.itemName = @"Apple Fritter";
    fritter.priceList = [NSArray arrayWithObjects:@"$2.25", nil];
    fritter.numOfSizes = 1;
    
    menuItem *cinRoll = [[menuItem alloc] init];
    cinRoll.itemName = @"Cinnamon Roll";
    cinRoll.priceList = [NSArray arrayWithObjects:@"$2.25", nil];
    cinRoll.numOfSizes = 1;
    
    NSArray *bakedArray = [NSArray arrayWithObjects:menuItemFour, menuItemFive, menuItemSeven,sunCake, croissant, muffin, scone, fritter, cinRoll, nil];
    
    [cinRoll release];
    [fritter release];
    [scone release];
    [muffin release];
    [croissant release];
    [sunCake release];
    [menuItemSeven release];
    [menuItemFive release];
    [menuItemFour release];
    
    
    menuItem *menuItemSix = [[menuItem alloc] init];
    menuItemSix.itemName = @"Caribbean Craze";
    menuItemSix.priceList = [NSArray arrayWithObjects:@"$1.75", @"$2.25", nil];
    menuItemSix.numOfSizes = 2;
    
    
    menuItem *strawKiwi = [[menuItem alloc] init];
    strawKiwi.itemName = @"Strawberry Kiwi";
    strawKiwi.priceList = menuItemSix.priceList;
    strawKiwi.numOfSizes = 2;
    //strawKiwi.ingredientList = menuItemSix.ingredientList;
    //strawKiwi.ingredientQuants = menuItemSix.ingredientQuants;
    
    menuItem *tropPine = [[menuItem alloc] init];
    tropPine.itemName = @"Tropical Pineapple";
    tropPine.priceList = menuItemSix.priceList;
    tropPine.numOfSizes = 2;
    
    menuItem *citMango = [[menuItem alloc] init];
    citMango.itemName = @"Citrus Mango";
    citMango.priceList = menuItemSix.priceList;
    citMango.numOfSizes = 2;
    
    menuItem *jamJam = [[menuItem alloc] init];
    jamJam.itemName = @"Jamaican Jammer";
    jamJam.priceList = menuItemSix.priceList;
    jamJam.numOfSizes = 2;
    jamJam.ingredientList = [NSArray arrayWithObjects:@"Boost", nil];
    jamJam.ingredientQuants = [NSArray arrayWithObjects:sixOne, nil];
    
    menuItem *mauiMango = [[menuItem alloc] init];
    mauiMango.itemName = @"Maui Mango";
    mauiMango.priceList = menuItemSix.priceList;
    mauiMango.numOfSizes = 2;
    
    menuItem *mysMango = [[menuItem alloc] init];
    mysMango.itemName = @"Mystic Mango";
    mysMango.priceList = menuItemSix.priceList;
    mysMango.numOfSizes = 2;
    
    menuItem *pineParadise = [[menuItem alloc] init];
    pineParadise.itemName = @"Pineapple Paradise";
    pineParadise.priceList = menuItemSix.priceList;
    pineParadise.numOfSizes = 2;
    
    menuItem *orngSun = [[menuItem alloc] init];
    orngSun.itemName = @"Orange Sunrise";
    orngSun.priceList = menuItemSix.priceList;
    orngSun.numOfSizes = 2;
    
    menuItem *strawSqueeze = [[menuItem alloc] init];
    strawSqueeze.itemName = @"Strawberry Squeeze";
    strawSqueeze.priceList = menuItemSix.priceList;
    strawSqueeze.numOfSizes = 2;
    
    menuItem *peachSun = [[menuItem alloc] init];
    peachSun.itemName = @"Peach Sunset";
    peachSun.priceList = menuItemSix.priceList;
    peachSun.numOfSizes = 2;
    
    menuItem *strawShoot = [[menuItem alloc] init];
    strawShoot.itemName = @"Strawberry Shooter";
    strawShoot.priceList = menuItemSix.priceList;
    strawShoot.numOfSizes = 2;
    
    
    NSArray *smoothieArray = [NSArray arrayWithObjects:menuItemSix,strawKiwi,tropPine, jamJam,mauiMango, mysMango, pineParadise, orngSun, strawSqueeze, peachSun, strawShoot, nil];
    
    [strawShoot release];
    [peachSun release];
    [strawSqueeze release];
    [orngSun release];
    [pineParadise release];
    [mysMango release];
    [mauiMango release];
    [jamJam release];
    [tropPine release];
    [strawKiwi release];
    [menuItemSix release];
    
    
    menuItem *bfstPanini = [[menuItem alloc] init];
    bfstPanini.itemName = @"Breakfast Panini";
    bfstPanini.priceList = [NSArray arrayWithObjects:@"$3.25", nil];
    bfstPanini.numOfSizes = 1;
    
    menuItem *bfstBurrito = [[menuItem alloc] init];
    bfstBurrito.itemName = @"Breakfast Burrito";
    bfstBurrito.priceList = [NSArray arrayWithObjects:@"$5.75", nil];
    bfstBurrito.numOfSizes = 1;
    
    menuItem *bocaBurger = [[menuItem alloc] init];
    bocaBurger.itemName = @"Boca Burger";
    bocaBurger.priceList = [NSArray arrayWithObjects:@"$5.25", nil];
    bocaBurger.numOfSizes = 1;
    
    menuItem *clscPanini = [[menuItem alloc] init];
    clscPanini.itemName = @"Clasico Panini";
    clscPanini.priceList = [NSArray arrayWithObjects:@"$5.50", nil];
    clscPanini.numOfSizes = 1;
    
    menuItem *ultPanini = [[menuItem alloc] init];
    ultPanini.itemName = @"Ultimo Panini";
    ultPanini.priceList = [NSArray arrayWithObjects:@"$5.50", nil];
    ultPanini.numOfSizes = 1;
    
    NSArray *sandwichArray = [NSArray arrayWithObjects:bfstPanini, bfstBurrito,bocaBurger, clscPanini, ultPanini, nil];
    
    [ultPanini release];
    [clscPanini release];
    [bocaBurger release];
    [bfstPanini release];
    [bfstBurrito release];
    
    menuItem *sdSalad = [[menuItem alloc] init];
    sdSalad.itemName = @"Side Salad";
    sdSalad.priceList = [NSArray arrayWithObjects:@"$4.50", nil];
    sdSalad.numOfSizes = 1;
    
    menuItem *caesar = [[menuItem alloc] init];
    caesar.itemName = @"Caesar Salad";
    caesar.priceList = [NSArray arrayWithObjects:@"$3.25", nil];
    caesar.numOfSizes = 1;
    
    menuItem *chkCaesarni = [[menuItem alloc] init];
    chkCaesarni.itemName = @"Chicken Caesar Salad";
    chkCaesarni.priceList = [NSArray arrayWithObjects:@"$5.75", nil];
    chkCaesarni.numOfSizes = 1;
    chkCaesarni.ingredientList = [NSArray arrayWithObjects:@"Anchovy Dressing", nil];
    chkCaesarni.ingredientQuants = [NSArray arrayWithObjects:nyQuants, nil];
    
    menuItem *chacha = [[menuItem alloc] init];
    chacha.itemName = @"Chinese Chicken Salad";
    chacha.priceList = [NSArray arrayWithObjects:@"$3.25", nil];
    chacha.numOfSizes = 1;
    
    
    
    NSArray *saladArray = [NSArray arrayWithObjects:sdSalad, caesar, chkCaesarni, chacha, nil];
    [chacha release];
    [chkCaesarni release];
    [caesar release];
    [sdSalad release];
    items = [NSArray arrayWithObjects:coffeeArray, espressoArray,smoothieArray, bakedArray, sandwichArray,saladArray, nil];

    [nyQuants release];
    [numQuants release];
    [espressoIList release];
    [dorp release];
    [darp release];
    [sixOne release];


    NSLog(@"Categories: %d", [categories retainCount]);
    NSLog(@"items: %d", [items retainCount]);
    NSLog(@"nyQuants: %d", [nyQuants retainCount]);
    //NSLog(@"numQuants: %d", [numQuants retainCount]);
    NSLog(@"espresslist: %d", [espressoIList retainCount]);
    //NSLog(@"sixOne: %d", [sixOne retainCount]);
    NSLog(@"coffees: %d", [coffeeArray retainCount]);
    NSLog(@"espressos: %d", [espressoArray retainCount]);
    NSLog(@"smoothies: %d", [smoothieArray retainCount]);
    NSLog(@"baked: %d", [bakedArray retainCount]);
    NSLog(@"sandwiches: %d", [sandwichArray retainCount]);
    NSLog(@"salads: %d", [saladArray retainCount]);
    


}

// -------------------------------------------------------------------------------------------------------
// Singleton stuff -- no need to ever modify code below here, it is correct.
// -------------------------------------------------------------------------------------------------------


+ (CBMenu *)sharedInstance {
	@synchronized(self) {
		if (sharedInstance == nil) {
			sharedInstance = [[CBMenu alloc] init];
		}
	}
	return sharedInstance;
}


+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (sharedInstance == nil) {
			sharedInstance = [super allocWithZone:zone];
			return sharedInstance; // assignment and return on first allocation
		}
	}
	return nil; // on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}


- (id)retain {
	return self;
}


- (unsigned)retainCount {
	return UINT_MAX; // denotes an object that cannot be released
}


- (oneway void)release {
	//do nothing
}


- (id)autorelease {
	return self;
}


@end
