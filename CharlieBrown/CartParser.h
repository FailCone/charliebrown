//
//  CartParser.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GDataXMLNode.h"
#import "cartItem.h"


@interface CartParser : NSObject {
    
    //Open the file if it exists, if not, make it.
    //Read in the information and return the array of cartItems from the file
    
    //Be able to write to the file
    
}

- (NSString *)dataFilePath:(BOOL)forSave; //Filepath for cart
- (NSString *)customsFilePath:(BOOL)forSave; //filepath for customizations
//The rest of these sound pretty self-explanitory:
- (NSMutableArray *)loadCart;
- (void)saveCartWithArray:(NSArray *) dataToSave withName:(NSString *)orderName;
- (NSMutableArray *)loadCustomItems;
- (void)saveCustomItemsArray:(NSArray *)dataToSave appending:(BOOL)append;


@end
