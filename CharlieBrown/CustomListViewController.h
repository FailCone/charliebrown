//
//  CustomListViewController.h
//  CharlieBrown
//
//  Created by Charles Semple on 5/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cartItem.h"
#import "CartParser.h"
#import "CartCell.h"
#import "FoodMenuTwoViewController.h"
#import "CBMenu.h"

@interface CustomListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UIButton *btnAddBtn;
    IBOutlet UITableView *custTable;
    
    NSMutableArray *itemsToShow;
    
}
- (IBAction)didTapAddBtn:(id)sender;
- (void)reloadItems;

@end
