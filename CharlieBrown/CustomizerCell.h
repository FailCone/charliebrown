//
//  CustomizerCell.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "menuItem.h"
@interface CustomizerCell : UITableViewCell {
    
IBOutlet UIButton *btnNextBtn;
    
    IBOutlet UILabel *custStatus;
    IBOutlet UILabel *custItem;
    
    int custState;
    
    NSArray *custStates;
    

}
//Init cell with a menuItem's custom items and with the options that correspond to it.
- (void)initCellWithAThing:(NSString *)thing withOptions:(NSArray *) options;

- (IBAction)userDidTapRight:(id)sender; //NEXT
- (IBAction)userDidTapLeft:(id)sender; //BACK

@property(nonatomic, retain) IBOutlet UIButton *btnPrevBtn;
@property(nonatomic, retain) IBOutlet UIButton *btnNextBtn;
@property(nonatomic, retain) IBOutlet UILabel *custStatus;
@property(nonatomic, retain) IBOutlet UILabel *custItem;
@property(nonatomic, retain) NSArray *custStates;
@property(nonatomic, assign) int custState;


@end
