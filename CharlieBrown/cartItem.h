//
//  cartItem.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "menuItem.h"

@interface cartItem : NSObject {
    
    NSString *itemName;
    NSString *itemPrice;
    NSString *custName;
    NSMutableArray *changeList;
    
    
}
//Function to return a string of all of the customizations
//to be displayed in a cell
-(NSString *) customString;

@property(nonatomic, retain) NSString *itemName;
@property(nonatomic, retain) NSString *itemPrice;
@property(nonatomic, retain) NSString *custName;
@property(nonatomic, retain) NSMutableArray *changeList;

@end
