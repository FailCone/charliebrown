//
//  CustomizerCell.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomizerCell.h"

@implementation CustomizerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initCellWithAThing:(NSString *)thing withOptions:(NSArray *)options{
    custItem.text = thing;
    custState = 0;
    custStates = options;
    custStatus.text = [custStates objectAtIndex:custState];
    [custStates retain];
    
}

@synthesize btnPrevBtn;
@synthesize btnNextBtn;
@synthesize custItem;
@synthesize custStatus;
@synthesize custStates;
@synthesize custState;

- (IBAction)userDidTapRight:(id)sender {
    if(custState == ([custStates count]-1)){
        custState = 0;
    } else {
        custState = custState+1;
    }
    custStatus.text = [custStates objectAtIndex:custState];

}

- (IBAction)userDidTapLeft:(id)sender {
    if(custState == 0){
        custState = ([custStates count] - 1);
    } else {
        custState = custState-1;
    }
    custStatus.text = [custStates objectAtIndex:custState];

}
@end
