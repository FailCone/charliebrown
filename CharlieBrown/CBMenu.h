//
//  CBMenu.h
//  MenuViewTwoTest
//
//  Created by Charles Semple on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "menuItem.h"

@interface CBMenu : NSObject {
    
    NSArray *categories;
    NSArray *items;
    
    
}

-(void) loadMenu;
-(NSArray *) returnCategories;
-(NSArray *) returnItemsWithCategory:(int) idx;
-(NSArray *) returnAllItems;
+ (CBMenu *)sharedInstance;             // get a pointer to this singleton class


@end
