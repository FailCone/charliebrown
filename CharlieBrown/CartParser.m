//
//  CartParser.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CartParser.h"

@implementation CartParser




- (NSString *)dataFilePath:(BOOL)forSave {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, 
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentsPath = [documentsDirectory
                               stringByAppendingPathComponent:@"cart.xml"];
    if (forSave || 
        [[NSFileManager defaultManager] fileExistsAtPath:documentsPath]) {
        return documentsPath;
    } else {
        return [[NSBundle mainBundle] pathForResource:@"cart" ofType:@"xml"];
    }
    
}

- (NSString *)customsFilePath:(BOOL)forSave {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, 
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentsPath = [documentsDirectory
                               stringByAppendingPathComponent:@"custs.xml"];
    if (forSave || 
        [[NSFileManager defaultManager] fileExistsAtPath:documentsPath]) {
        return documentsPath;
    } else {
        return [[NSBundle mainBundle] pathForResource:@"cust" ofType:@"xml"];
    }
    
}

- (NSMutableArray *)loadCart {
    
    NSString *filePath = [self dataFilePath:FALSE];
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData 
                                                           options:0 error:&error];
    if (doc == nil) {
        [xmlData release];    
        return nil; 
    }
    
    //Load from the cart
    NSArray *cartArray = [doc.rootElement elementsForName:@"item"];
    NSMutableArray *returnArray = [[NSMutableArray alloc]init];
    for(GDataXMLElement *itemElement in cartArray) {
        cartItem *tempItem = [[cartItem alloc] init];
        
        
        GDataXMLElement *tempNameElement = [[itemElement elementsForName:@"itemName"]objectAtIndex:0];
        tempItem.itemName = tempNameElement.stringValue;
        
        GDataXMLElement *tempPriceElement = [[itemElement elementsForName:@"price"]objectAtIndex:0];
        tempItem.itemPrice = tempPriceElement.stringValue;
        
        
        NSArray *customs = [itemElement elementsForName:@"custom"];
        for(int i = 0; i < [customs count]; i++){
            GDataXMLElement *tempCustomElement = [customs objectAtIndex:i];
            [tempItem.changeList addObject:tempCustomElement.stringValue];
        }
        
        [returnArray addObject:tempItem];
        [tempItem release];
    }
    [doc release];
    [xmlData release];
    //It says "POTENTIAL" Leak...
    return returnArray;
    
    
}

- (NSMutableArray *) loadCustomItems {
    
    NSString *filePath = [self customsFilePath:FALSE];
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData 
                                                           options:0 error:&error];
    if (doc == nil) { 
        [xmlData release];
        return nil;
    }
    
    //Load from the cart
    NSArray *cartArray = [doc.rootElement elementsForName:@"item"];
    NSMutableArray *returnArray = [[NSMutableArray alloc]init];
    for(GDataXMLElement *itemElement in cartArray) {
        cartItem *tempItem = [[cartItem alloc] init];
        
        
        GDataXMLElement *tempNameElement = [[itemElement elementsForName:@"itemName"]objectAtIndex:0];
        tempItem.itemName = tempNameElement.stringValue;
        
        GDataXMLElement *tempcNameElement = [[itemElement elementsForName:@"custName"]objectAtIndex:0];
        tempItem.custName = tempcNameElement.stringValue;
        
        GDataXMLElement *tempPriceElement = [[itemElement elementsForName:@"price"]objectAtIndex:0];
        tempItem.itemPrice = tempPriceElement.stringValue;
        
        
        NSArray *customs = [itemElement elementsForName:@"custom"];
        for(int i = 0; i < [customs count]; i++){
            GDataXMLElement *tempCustomElement = [customs objectAtIndex:i];
            [tempItem.changeList addObject:tempCustomElement.stringValue];
        }
        
        [returnArray addObject:tempItem];
        [tempItem release];
    }
    [doc release];
    [xmlData release];
    return returnArray;
    
    

}

- (void) saveCartWithArray:(NSArray *)dataToSave withName:(NSString *)orderName {
    NSLog(@"saveCartWithArray");
    GDataXMLElement *cartElement = [GDataXMLElement elementWithName:@"cart"];
    if(orderName != nil){
        GDataXMLElement *orderNameElement = [GDataXMLElement elementWithName:@"orderName" stringValue:orderName];
        [cartElement addChild:orderNameElement];
    }
    for(cartItem *item in dataToSave){
        GDataXMLElement *itemElement = [GDataXMLElement elementWithName:@"item"];
        GDataXMLElement *nameElement = [GDataXMLElement elementWithName:@"itemName" stringValue:item.itemName];
        GDataXMLElement *priceElement = [GDataXMLElement elementWithName:@"price" stringValue:item.itemPrice];
        
        [itemElement addChild:nameElement];
        [itemElement addChild:priceElement];
        for(NSString *cust in item.changeList){
            GDataXMLElement *customElement = [GDataXMLElement elementWithName:@"custom" stringValue:cust];
            [itemElement addChild:customElement];
        }
        [cartElement addChild:itemElement];
    }
    GDataXMLDocument *document = [[[GDataXMLDocument alloc]initWithRootElement:cartElement] autorelease];
    
    NSData *xmldata = document.XMLData;
    NSString *filepath = [self dataFilePath:TRUE];
    NSLog(@"Saving to %@", filepath);
    [xmldata writeToFile:filepath atomically:YES];
      
}

-(void) saveCustomItemsArray:(NSArray *)dataToSave appending:(BOOL)append{
    NSMutableArray *loadedData;
    if(append){
        loadedData = [NSMutableArray arrayWithArray:[self loadCustomItems]];
        if(loadedData == nil){
            //loadedData = [[NSMutableArray alloc]init];
        }
        [loadedData addObjectsFromArray:dataToSave];
    } else {
        loadedData = [NSMutableArray arrayWithArray:dataToSave];
    }
    
    NSLog(@"saveCartWithArray");
    GDataXMLElement *cartElement = [GDataXMLElement elementWithName:@"cart"];
    
    for(cartItem *item in loadedData){
        GDataXMLElement *itemElement = [GDataXMLElement elementWithName:@"item"];
        GDataXMLElement *cNameElement = [GDataXMLElement elementWithName:@"custName"];
        GDataXMLElement *nameElement = [GDataXMLElement elementWithName:@"itemName" stringValue:item.itemName];
        GDataXMLElement *priceElement = [GDataXMLElement elementWithName:@"price" stringValue:item.itemPrice];
        
        [itemElement addChild:nameElement];
        [itemElement addChild:cNameElement];
        [itemElement addChild:priceElement];
        for(NSString *cust in item.changeList){
            GDataXMLElement *customElement = [GDataXMLElement elementWithName:@"custom" stringValue:cust];
            [itemElement addChild:customElement];
        }
        [cartElement addChild:itemElement];
    }
    GDataXMLDocument *document = [[[GDataXMLDocument alloc]initWithRootElement:cartElement] autorelease];
    
    NSData *xmldata = document.XMLData;
    NSString *filepath = [self customsFilePath:TRUE];

    NSLog(@"Saving to %@", filepath);
    [xmldata writeToFile:filepath atomically:YES];
    
    
}

@end
