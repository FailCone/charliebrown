//
//  MainMenuViewController.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
// EDITEDTI

#import "MainMenuViewController.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Charlie Brown's";
}

- (void)viewDidUnload
{
    [imvFoodMenuIcon release];
    imvFoodMenuIcon = nil;
    [imvOrderMenuIcon release];
    imvOrderMenuIcon = nil;
    [imvCustomMenuIcon release];
    imvCustomMenuIcon = nil;
    [imvOrderMenuIcon release];
    imvOrderMenuIcon = nil;
    [imvCustomMenuIcon release];
    imvCustomMenuIcon = nil;
    [imvContactMenuIcon release];
    imvContactMenuIcon = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(CGRectContainsPoint(imvFoodMenuIcon.frame, touchLocation)){
        CBMenu *WholeMenu = [CBMenu sharedInstance];
        NSLog(@"MENU");
        NSArray *cats = [WholeMenu returnCategories];
        NSLog(@"Cats");
        NSArray *menu = [WholeMenu returnAllItems];
        NSLog(@"Menu");
        
        //What are these I don't even
        //[cats retain];
        //[menu retain];
        
        FoodMenuTwoViewController *mmvc = [[FoodMenuTwoViewController alloc]initWithData:cats :NO: YES];
        [mmvc setDataSource:cats];
        [mmvc setMenu:menu];
        [mmvc setTitle:@"Menu"];
        [self.navigationController pushViewController:mmvc animated:YES];

        //FOR SOME REASON IT DOESN'T LIKE ME RELEASING CATS AND MENU OR REMOVING THEIR
        //RETAIN STATEMENTS ABOVE PLEASE DON'T HURT ME.
        [mmvc release];
        return;
    }
    
    if(CGRectContainsPoint(imvOrderMenuIcon.frame, touchLocation)){
        CartViewController *cvc = [[CartViewController alloc]initWithCartItem:nil];
        [self.navigationController pushViewController:cvc animated:YES];
        [cvc release];
        return;
    }
    
    if(CGRectContainsPoint(imvCustomMenuIcon.frame, touchLocation)){
        CustomListViewController *clvc = [[CustomListViewController alloc]init];
        [self.navigationController pushViewController:clvc animated:YES];
        [clvc release];
    }
    if(CGRectContainsPoint(imvContactMenuIcon.frame, touchLocation)){
        ContactUsViewController *cuvc = [[ContactUsViewController alloc]init];
        [self.navigationController pushViewController:cuvc animated:YES];
        [cuvc release];
    }
    
}


- (void)dealloc {
    [imvFoodMenuIcon release];
    [imvOrderMenuIcon release];
    [imvCustomMenuIcon release];
    [imvOrderMenuIcon release];
    [imvCustomMenuIcon release];
    [imvContactMenuIcon release];
    [imvContactMenuIcon release];
    [super dealloc];
}
@end
