//
//  CartCell.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CartCell.h"

@implementation CartCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) initWithCartItem:(cartItem *)item{
    
    lblName.text = item.itemName;
    lblPrice.text = item.itemPrice;
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@synthesize lblName;
@synthesize lblPrice;
@synthesize lblChanges;

- (void)dealloc {
    [lblName release];
    [lblPrice release];
    [lblChanges release];
    [super dealloc];
}
@end
