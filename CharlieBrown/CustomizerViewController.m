//
//  CustomizerViewController.m
//  CharlieBrown
//
//  Created by Charles Semple on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomizerViewController.h"

@interface CustomizerViewController ()

@end

@implementation CustomizerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    custTable.dataSource = self;
    custTable.delegate = self;
    self.title = @"Customize/Confirm";
    sizeSelected = 0;
    cellStorage = [[NSMutableArray alloc] init];
    
    //Making adjustments based on if we're customizing and wany buttons
    //in certain places or not
    if(item.numOfSizes > 1){
        custTable.frame = CGRectMake(custTable.frame.origin.x, 
                                     custTable.frame.origin.y, 
                                     custTable.frame.size.width,
                                     custTable.frame.size.height-50);
        segSizeSelect.userInteractionEnabled = YES;
        segSizeSelect.hidden = NO;
        if(item.numOfSizes > 2){
            [segSizeSelect insertSegmentWithTitle:nil atIndex:2 animated:NO];
            [segSizeSelect setTitle:@"Small" forSegmentAtIndex:0];
            [segSizeSelect setTitle:@"Medium" forSegmentAtIndex:1];
            [segSizeSelect setTitle:@"Large" forSegmentAtIndex:2];
        } else {
            if([item.itemName isEqualToString:@"Espresso"]){
                [segSizeSelect setTitle:@"Single" forSegmentAtIndex:0];
                [segSizeSelect setTitle:@"Double" forSegmentAtIndex:1];
            } else {
                [segSizeSelect setTitle:@"Regular" forSegmentAtIndex:0];
                [segSizeSelect setTitle:@"Large" forSegmentAtIndex:1];
   
            }
        }
        
    }
    if([item.ingredientList count] == 0){
        UILabel *noCustomLabel = [[UILabel alloc]init];
        noCustomLabel.frame = [custTable frame];
        noCustomLabel.backgroundColor = [UIColor darkGrayColor];
        noCustomLabel.textAlignment = UITextAlignmentCenter;
        noCustomLabel.textColor = [UIColor whiteColor];
        noCustomLabel.text = @"NO CUSTOMIZATIONS AVAILABLE";
        if(customizerMode){
            [btnConfirmBtn removeFromSuperview];
            btnCancelBtn.center = CGPointMake(320/2, btnCancelBtn.center.y);
        } 
        [self.view addSubview:noCustomLabel];
        [noCustomLabel release];
    } else if (customizerMode){
        [btnConfirmBtn setImage:[UIImage imageNamed:@"saveButan.png"] forState:UIControlStateNormal];
    }
    
     
}

//Init witha food item to be customized and if it's going to be saved or carted
- (id) initWithFoodItem:(menuItem *)inputItem:(bool)inCustomMode{
    customizerMode = inCustomMode;
    item = inputItem;
    [custTable reloadData];
    [item retain];
    return self;
}

//User taps Cancel: GO BACK
- (IBAction)didTapCancelBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//If the user taps the add/save button: 
//If we're customizing items for storage: store the item and go back
//If we're buyin' stuff: send the item to the card to be added there
- (IBAction)didTapAddBtn:(id)sender {
    if(!customizerMode){
        //Make a cartItem
        cartItem *tempCartItem = [self createCartItem];
        [tempCartItem retain];
        
        CartViewController *cartvc = [[CartViewController alloc] initWithCartItem:tempCartItem];
        //[cartvc updateCart];
        [self.navigationController pushViewController:cartvc animated:YES];
    } else {
        cartItem *tempCartItem = [self createCartItem];
        [tempCartItem retain];
        CartParser *parser = [[CartParser alloc]init];
        NSArray *arr = [NSArray arrayWithObjects:tempCartItem, nil];
        [parser saveCustomItemsArray:arr appending:YES];
        NSMutableArray *vcs = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for(int i = [vcs count]-1; i > 0; i--){
            [vcs removeObjectAtIndex:i];
        }
        [vcs addObject:[[CustomListViewController alloc]init]];
    self.navigationController.viewControllers = vcs;
    }
}

//Generate a cartItem object that only has the attributes of the given item 
//that the people who are actually dealing with the food care about
- (cartItem *)createCartItem {
    
    cartItem *returnItem = [[cartItem alloc]init];
    returnItem.itemName = item.itemName;
    returnItem.itemPrice = [item.priceList objectAtIndex:0];
    if(item.numOfSizes > 1){
        NSString *sizeString;
        if(item.numOfSizes == 2){
            if([item.itemName isEqualToString:@"Espresso"]){
                if(segSizeSelect.selectedSegmentIndex == 0){
                    sizeString = @"(Sgl)";
                    returnItem.itemPrice = [item.priceList objectAtIndex:0];
                }
                if(segSizeSelect.selectedSegmentIndex == 1){
                    sizeString = @"(Dbl)";
                    returnItem.itemPrice = [item.priceList objectAtIndex:1];
                }
            } else {
                if(segSizeSelect.selectedSegmentIndex == 0){
                    sizeString = @"(Reg)";
                    returnItem.itemPrice = [item.priceList objectAtIndex:0];
                }
                if(segSizeSelect.selectedSegmentIndex == 1){
                    sizeString = @"(Lrg)";
                    returnItem.itemPrice = [item.priceList objectAtIndex:1];
                } 
            }
        }
        if(item.numOfSizes == 3){
            if(segSizeSelect.selectedSegmentIndex == 0){
                sizeString = @"(Sm)";
                returnItem.itemPrice = [item.priceList objectAtIndex:0];
            }
            if(segSizeSelect.selectedSegmentIndex == 1){
                sizeString = @"(Med)";
                returnItem.itemPrice = [item.priceList objectAtIndex:1];
            }
            if(segSizeSelect.selectedSegmentIndex == 2){
                sizeString = @"(Lrg)";
                returnItem.itemPrice = [item.priceList objectAtIndex:2];
            }
        }
        //[returnItem.itemName stringByAppendingString:sizeString];
        returnItem.itemName = [NSString stringWithFormat:@"%@ %@", item.itemName, sizeString];
    }

    NSArray *allCells = [custTable visibleCells];
    for(int i = 0; i < [allCells count]; i++){
        CustomizerCell *cell = [allCells objectAtIndex:i];
        //CustomizerCell *sell = [cellStorage objectAtIndex:i];
            NSString *tempCust = [NSString stringWithFormat:@"%@ - %@", cell.custItem.text, [cell.custStates objectAtIndex:cell.custState]];
            [returnItem.changeList addObject:tempCust];
        
    }
    
    return returnItem;   
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [item.ingredientList count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //I don't think I want anything here
    return;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleDelete;    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CustomizerCell";
     CustomizerCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //assert([item.ingredientList objectAtIndex:indexPath.row]);
    // Configure the cell...
    if(cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomizerCell" owner:nil options:nil];
        
        for (id currentObject in topLevelObjects){
            if([currentObject isKindOfClass:[UITableViewCell class]]){
                cell = (CustomizerCell *) currentObject;
                break;
            }
        }
        
    }
    cell.custItem.text = @"Tomato";
    //cell.custStatus.text = @"Herp";
    int num = indexPath.row;
    [cell initCellWithAThing:[item.ingredientList objectAtIndex:num] withOptions:[item.ingredientQuants objectAtIndex:num]];
    [cellStorage addObject:cell];
    return cell;
    
}



- (void)viewDidUnload
{
    [custTable release];
    custTable = nil;
    [btnCancelBtn release];
    btnCancelBtn = nil;
    [btnConfirmBtn release];
    btnConfirmBtn = nil;
    [segSizeSelect release];
    segSizeSelect = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [custTable release];
    [btnCancelBtn release];
    [btnConfirmBtn release];
    [segSizeSelect release];
    [super dealloc];
}

@synthesize custTable;
@end
