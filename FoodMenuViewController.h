//
//  FoodMenuViewController.h
//  CharlieBrown
//
//  Created by Charles Semple on 4/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "menuItem.h"
#import "MenuItemViewController.h"
#import "CustomizerViewController.h"



@interface FoodMenuViewController : UITableViewController {
    
    NSMutableArray *WholeMenu;
    
    NSArray *listOfCategories;
    
    bool customMode;
    bool categoryMode;
    
}

- (void)setModeCustom;
- (id) initWithModeCustom:(bool)custom;

@property(nonatomic,assign)bool customMode;

@end
